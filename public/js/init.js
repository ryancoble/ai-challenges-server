
$(function() {
	const port = 4000;
	const socket = io.connect('http://127.0.0.1:' + port);
	const challengeKey = $('body').attr('data-challenge');

	// when connected to server
	socket.on('connect', function() {
		console.log('connected to server!');
	});

	// when server sends handshake
	socket.on('message', function(message) {
		switch(message.message_id) {
			case 'handshake':
			// send the handshake
			socket.emit('message', {
				message_id: 'handshake',
				body: {
					client_type: 'spectator',
					challenge_key: challengeKey
				}
			});
			break;

			case 'update_ui':
				switch(message.body.update_key) {
					case 'add_client':
					if(socket.id !== message.body.data.client_id) {
						console.log('add client', message.body.data);
					}
					break;
					case 'remove_client':
					console.log('remove client', message.body.data);
					break;
					case 'start_challenge':
					$('.start-challenge').html('Challenge In-Progress');
					$('.start-challenge').prop('disabled', true);
					console.log('start challenge', message.body.data);
					break;
					case 'competitor_update':
					console.log('competitor update', message.body.data);
					break;
					case 'end_challenge':
					$('.start-challenge').html('Start Challenge');
					$('.start-challenge').prop('disabled', false);
					console.log('end challenge', message.body.data);
					break;
				}
			break;
		}
	})

	// when ui data is sent from server (includes contestants, viewers and prediction results)
	//socket.on('update_ui', function(data) {
		/*const update_contestants = (typeof data.contestants !== 'undefined');
		const update_viewers     = (typeof data.viewers !== 'undefined');
		const update_graph       = (typeof data.problems !== 'undefined');

		const options = {
			contestants: update_contestants,
			viewers:     update_viewers,
			graph:       update_graph
		};

		UI.renderer.update(options, data);*/

		//console.log(data);
	//});

	// send start challenge when start challenge button
	$(document).on('click', '.start-challenge', function(e) {
		e.preventDefault();
		socket.emit('message', {
			message_id: 'start_challenge',
			body: {
				challenge_key: challengeKey
			}
		});
	})
});