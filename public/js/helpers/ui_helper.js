let UI = {};

function Renderer() {}

Renderer.prototype.drawContestants = function(contestants) {
	$('#contestants').html('');
	contestants.forEach(function(contestant, index) {
		$('#contestants').append('<li>Contestant ' + contestant + '</li>');
	});

	if(!contestants.length) {
		$('#contestants').html('No contestants');
	}
};
Renderer.prototype.drawViewers = function(viewers) {
	$('#viewers').html('');
	viewers.forEach(function(viewer, index) {
		$('#viewers').append('<li>Viewer ' + viewer + '</li>');
	});

	if(!viewers.length) {
		$('#viewers').html('No viewers');
	}
};
Renderer.prototype.drawGraph = function(problems) {
	let graph = new Graph(problems);
	graph.renderAccuracyChart();
};

Renderer.prototype.update = function(options, data) {
	let update_contestants = options.contestants;
	let update_viewers     = options.viewers;
	let update_graph       = options.graph;

	if(update_contestants) this.drawContestants(data.contestants);
	if(update_viewers)     this.drawViewers(data.viewers);
	if(update_graph)       this.drawGraph(data.problems);
};

UI.renderer = new Renderer();