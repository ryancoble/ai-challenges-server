function Graph(data) {
	this.data = data;
}

Graph.prototype.renderAccuracyChart = function() {

	console.log(this.data);

	let outerWidth  = 500;
	let outerHeight = 250;
	let margin = {
		left: 80,
		top: 5,
		right: 100,
		bottom: 60
	};

	let xColumn     = 'timestamp';
	let yColumn     = 'error';
	let colorColumn = 'contestant';
	let lineColumn  = colorColumn;

	let xAxisLabelText   = 'Time';
	let xAxisLabelOffset = 48;
	let yAxisLabelText   = 'Error';
	let yAxisLabelOffset = 60;

	let innerWidth  = outerWidth - (margin.left - margin.right);
	let innerHeight = outerHeight - (margin.top - margin.bottom);

	let svg = d3.select('#chart')
		.append('svg')
		.attr('width', outerWidth)
		.attr('height', outerHeight);
	let g = svg.append('g')
		.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

	let xAxisG      = g.append('g')
		.attr('class', 'x axis')
		.attr('transform', 'translate(0,' + innerHeight + ')');
	let xAxisLabel  = xAxisG.append('text')
		.style('text-anchor', 'middle')
		.attr('transform', 'translate(' + (innerHeight / 2) + ',' + xAxisLabelOffset + ')')
		.attr('class', 'label')
		.text(xAxisLabelText);

	let yAxisG      = g.append('g')
		.attr('class', 'y axis');
	let yAxisLabel  = yAxisG.append('text')
		.style('text-anchor', 'middle')
		.attr('transform', 'translate(-' + yAxisLabelOffset + ',' + (innerHeight / 2) + ') rotate(-90)')
		.attr('class', 'label')
		.text(yAxisLabelText);

	let colorLegendG = g.append('g')
		.attr('class', 'color-legend')
		.attr('transform', 'translate(325, 20)');

	let xScale = d3.time.scale().range([0, innerWidth]);
	let yScale = d3.scale.linear().range([innerHeight, 0]);
	let colorScale = d3.scale.category10();

	let xAxis = d3.svg.axis()
		.scale(xScale)
		.orient('bottom')
		.ticks(5)
		.outerTickSize(0);
	let yAxis = d3.svg.axis()
		.scale(yScale)
		.orient('left')
		.ticks(5)
		.outerTickSize(0);

	let line = d3.svg.line()
		.x((d) => xScale(d[xColumn]))
		.y((d) => yScale(d[yColumn]));

	let colorLegend = d3.legend.color()
		.scale(colorScale)
		.shapePadding(3)
		.shapeWidth(15)
		.shapeHeight(15)
		.labelOffset(4);


	function render(data) {
		xScale.domain(d3.extent(data, (d) => d[xColumn]));
		yScale.domain([0, d3.max(data, (d) => d[yColumn])]);

		xAxisG.call(xAxis);
		yAxisG.call(yAxis);

		let nested = d3.nest()
			.key((d) => d[lineColumn])
			.entries(data);

		colorScale.domain(nested.map((d) => d.key));

		let paths = g.selectAll('.chart-line').data(nested);
		paths.enter().append('path').attr('class', 'chart-line');
		paths.exit().remove();
		paths
			.attr('d', (d) => line(d.values))
			.attr('stroke', (d) => colorScale(d.key));

		colorLegendG.call(colorLegend);
	}

	if(this.data && this.data.length) {
		render(this.data);
	}
};