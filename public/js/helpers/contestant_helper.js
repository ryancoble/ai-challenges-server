function ContestantRepository() {
	this.contestants = [];
};
ContestantRepository.prototype.get = function() {
	return this.contestants;
};
ContestantRepository.prototype.has = function(contestantId) {
	return typeof this.contestants !== 'undefined' ? true : false;
};
ContestantRepository.prototype.remove = function(contestantId) {
	delete this.contestants[contestantId];
};
ContestantRepository.prototype.add = function(contestant) {
	this.contestants.push(contestant);
};