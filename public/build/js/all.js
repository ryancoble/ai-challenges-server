function ContestantRepository() {
	this.contestants = [];
};
ContestantRepository.prototype.get = function() {
	return this.contestants;
};
ContestantRepository.prototype.has = function(contestantId) {
	return typeof this.contestants !== 'undefined' ? true : false;
};
ContestantRepository.prototype.remove = function(contestantId) {
	delete this.contestants[contestantId];
};
ContestantRepository.prototype.add = function(contestant) {
	this.contestants.push(contestant);
};
function Graph(data) {
	this.data = data;
}

Graph.prototype.renderAccuracyChart = function() {

	console.log(this.data);

	let outerWidth  = 500;
	let outerHeight = 250;
	let margin = {
		left: 80,
		top: 5,
		right: 100,
		bottom: 60
	};

	let xColumn     = 'timestamp';
	let yColumn     = 'error';
	let colorColumn = 'contestant';
	let lineColumn  = colorColumn;

	let xAxisLabelText   = 'Time';
	let xAxisLabelOffset = 48;
	let yAxisLabelText   = 'Error';
	let yAxisLabelOffset = 60;

	let innerWidth  = outerWidth - (margin.left - margin.right);
	let innerHeight = outerHeight - (margin.top - margin.bottom);

	let svg = d3.select('#chart')
		.append('svg')
		.attr('width', outerWidth)
		.attr('height', outerHeight);
	let g = svg.append('g')
		.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

	let xAxisG      = g.append('g')
		.attr('class', 'x axis')
		.attr('transform', 'translate(0,' + innerHeight + ')');
	let xAxisLabel  = xAxisG.append('text')
		.style('text-anchor', 'middle')
		.attr('transform', 'translate(' + (innerHeight / 2) + ',' + xAxisLabelOffset + ')')
		.attr('class', 'label')
		.text(xAxisLabelText);

	let yAxisG      = g.append('g')
		.attr('class', 'y axis');
	let yAxisLabel  = yAxisG.append('text')
		.style('text-anchor', 'middle')
		.attr('transform', 'translate(-' + yAxisLabelOffset + ',' + (innerHeight / 2) + ') rotate(-90)')
		.attr('class', 'label')
		.text(yAxisLabelText);

	let colorLegendG = g.append('g')
		.attr('class', 'color-legend')
		.attr('transform', 'translate(325, 20)');

	let xScale = d3.time.scale().range([0, innerWidth]);
	let yScale = d3.scale.linear().range([innerHeight, 0]);
	let colorScale = d3.scale.category10();

	let xAxis = d3.svg.axis()
		.scale(xScale)
		.orient('bottom')
		.ticks(5)
		.outerTickSize(0);
	let yAxis = d3.svg.axis()
		.scale(yScale)
		.orient('left')
		.ticks(5)
		.outerTickSize(0);

	let line = d3.svg.line()
		.x((d) => xScale(d[xColumn]))
		.y((d) => yScale(d[yColumn]));

	let colorLegend = d3.legend.color()
		.scale(colorScale)
		.shapePadding(3)
		.shapeWidth(15)
		.shapeHeight(15)
		.labelOffset(4);


	function render(data) {
		xScale.domain(d3.extent(data, (d) => d[xColumn]));
		yScale.domain([0, d3.max(data, (d) => d[yColumn])]);

		xAxisG.call(xAxis);
		yAxisG.call(yAxis);

		let nested = d3.nest()
			.key((d) => d[lineColumn])
			.entries(data);

		colorScale.domain(nested.map((d) => d.key));

		let paths = g.selectAll('.chart-line').data(nested);
		paths.enter().append('path').attr('class', 'chart-line');
		paths.exit().remove();
		paths
			.attr('d', (d) => line(d.values))
			.attr('stroke', (d) => colorScale(d.key));

		colorLegendG.call(colorLegend);
	}

	if(this.data && this.data.length) {
		render(this.data);
	}
};
let UI = {};

function Renderer() {}

Renderer.prototype.drawContestants = function(contestants) {
	$('#contestants').html('');
	contestants.forEach(function(contestant, index) {
		$('#contestants').append('<li>Contestant ' + contestant + '</li>');
	});

	if(!contestants.length) {
		$('#contestants').html('No contestants');
	}
};
Renderer.prototype.drawViewers = function(viewers) {
	$('#viewers').html('');
	viewers.forEach(function(viewer, index) {
		$('#viewers').append('<li>Viewer ' + viewer + '</li>');
	});

	if(!viewers.length) {
		$('#viewers').html('No viewers');
	}
};
Renderer.prototype.drawGraph = function(problems) {
	let graph = new Graph(problems);
	graph.renderAccuracyChart();
};

Renderer.prototype.update = function(options, data) {
	let update_contestants = options.contestants;
	let update_viewers     = options.viewers;
	let update_graph       = options.graph;

	if(update_contestants) this.drawContestants(data.contestants);
	if(update_viewers)     this.drawViewers(data.viewers);
	if(update_graph)       this.drawGraph(data.problems);
};

UI.renderer = new Renderer();

$(function() {
	const port = 4000;
	const socket = io.connect('http://127.0.0.1:' + port);
	const challengeKey = $('body').attr('data-challenge');

	// when connected to server
	socket.on('connect', function() {
		console.log('connected to server!');
	});

	// when server sends handshake
	socket.on('message', function(message) {
		switch(message.message_id) {
			case 'handshake':
			// send the handshake
			socket.emit('message', {
				message_id: 'handshake',
				body: {
					client_type: 'spectator',
					challenge_key: challengeKey
				}
			});
			break;

			case 'update_ui':
				switch(message.body.update_key) {
					case 'add_client':
					if(socket.id !== message.body.data.client_id) {
						console.log('add client', message.body.data);
					}
					break;
					case 'remove_client':
					console.log('remove client', message.body.data);
					break;
					case 'start_challenge':
					$('.start-challenge').html('Challenge In-Progress');
					$('.start-challenge').prop('disabled', true);
					console.log('start challenge', message.body.data);
					break;
					case 'competitor_update':
					console.log('competitor update', message.body.data);
					break;
					case 'end_challenge':
					$('.start-challenge').html('Start Challenge');
					$('.start-challenge').prop('disabled', false);
					console.log('end challenge', message.body.data);
					break;
				}
			break;
		}
	})

	// when ui data is sent from server (includes contestants, viewers and prediction results)
	//socket.on('update_ui', function(data) {
		/*const update_contestants = (typeof data.contestants !== 'undefined');
		const update_viewers     = (typeof data.viewers !== 'undefined');
		const update_graph       = (typeof data.problems !== 'undefined');

		const options = {
			contestants: update_contestants,
			viewers:     update_viewers,
			graph:       update_graph
		};

		UI.renderer.update(options, data);*/

		//console.log(data);
	//});

	// send start challenge when start challenge button
	$(document).on('click', '.start-challenge', function(e) {
		e.preventDefault();
		socket.emit('message', {
			message_id: 'start_challenge',
			body: {
				challenge_key: challengeKey
			}
		});
	})
});