'use strict';

function ChallengeManager() {
	this.challenges = {};
}

// get all challenges
ChallengeManager.prototype.getAllChallenges = function() {
	return this.challenges;
};

// get all challenges that have been started
ChallengeManager.prototype.getStartedChallenges = function() {
	let activeChallenges = {};
	for(let cIndex in this.challenges) {
		if(this.challenges[cKey].status === 'started') {
			activeChallenges[cKey] = this.challenges[cKey];
		}
	}
	return activeChallenges;
};

// get a challenge by key
ChallengeManager.prototype.getChallengeByKey = function(key) {
	if(typeof this.challenges[key] !== 'undefined') {
		return this.challenges[key];
	}
	return false;
};

// set a challenge status
ChallengeManager.prototype.setChallengeStatus = function(key, status) {
	// not found
	if(typeof this.challenges[key] === 'undefined') {
		return false;
	}
	this.challenges[key].status = status;
	return true;
};

// setup a challenge (problems all round)
ChallengeManager.prototype.setupChallenge = function(key, problems) {
	// not found
	if(typeof this.challenges[key] === 'undefined') {
		return false;
	}

	this.challenges[key].problems = problems;
	return true;
};

// get round data
ChallengeManager.prototype.getProblemSet = function(key, round) {
	if(typeof this.challenges[key] === 'undefined') {
		return false;
	}

	let problems = this.challenges[key].problems;
	if(typeof problems[round - 1] === 'undefined') {
		return false;
	}

	return problems[round - 1];
};

// add a challenge
ChallengeManager.prototype.addChallenge = function(challenge) {
	// add challenge to challenges list
	if(typeof this.challenges[challenge.key] === 'undefined') {
		challenge.problems = challenge.problems || [];
		this.challenges[challenge.key] = challenge;
		return true;
	}
	return false;
};

// remove a challenge using key
ChallengeManager.prototype.removeChallenge = function(key) {
	if(typeof this.challenges[key] !== 'undefined') {
		delete this.challenges[key];
		return true;
	}
	return false;
};


module.exports = ChallengeManager;