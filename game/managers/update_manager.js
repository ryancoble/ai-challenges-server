'use strict';

function Queue(data) {
	this.items = [];
	if(data instanceof Array) {
		this.items = data;
	}

	this.length = this.items.length;

	this.enqueue = function(item) {
		this.length += 1;
		return this.items.push(item);
	};

	this.dequeue = function() {
		if(this.length > 0) {
			this.length -= 1;
			return this.items.shift();
		}
		return false;
	}
}


function UpdateManager() {
	this.updates = new Queue();
}

UpdateManager.prototype.makeUpdate = function(updateKey, data) {
	return {
		timestamp: Date.now(),
		update_key: updateKey,
		data: data
	};
};

UpdateManager.prototype.pop = function() {
	if(!this.updates.length) {
		return [];
	}
	return this.updates.dequeue();
};

UpdateManager.prototype.push = function(update) {
	return this.updates.enqueue(update);
};

module.exports = UpdateManager;