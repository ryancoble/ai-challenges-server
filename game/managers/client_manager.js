'use strict';

function ClientManager() {
	this.clients = {};
}

// get all clients on the server
ClientManager.prototype.getAllClients = function() {
	return this.clients;
};

// get all clients for a given challenge by key
ClientManager.prototype.getAllClientsByChallengeKey = function(challengeKey) {
	let clients = [];
	for(let clientIndex in this.clients) {
		if(this.clients[clientIndex].challenge_key === challengeKey) {
			clients.push(this.clients[clientIndex]);
		}
	}
	return clients;
};

// get all competitors for a given challenge by key
ClientManager.prototype.getCompetitors = function(challengeKey) {
	let clients = [];
	for(let clientIndex in this.clients) {
		if(this.clients[clientIndex].challenge_key === challengeKey && this.clients[clientIndex].type === 'competitor') {
			clients.push(this.clients[clientIndex]);
		}
	}
	return clients;
};

// get all spectators for a given challenge by key
ClientManager.prototype.getSpectators = function(challengeKey) {
	let clients = [];
	for(let clientIndex in this.clients) {
		if(this.clients[clientIndex].challenge_key === challengeKey && this.clients[clientIndex].type === 'spectator') {
			clients.push(this.clients[clientIndex]);
		}
	}
	return clients;
};

// get a client by socket id
ClientManager.prototype.getClient = function(socketId) {
	if(typeof this.clients[socketId] !== 'undefined') {
		return this.clients[socketId];
	}
	return false;
};

// get a client with highest score
ClientManager.prototype.getWinner = function(challengeKey) {
	let winnerClient = null;
	let highestScore = 0;
	let clients = this.getCompetitors(challengeKey);
	for(let clientIndex in clients) {
		let client = clients[clientIndex];
		if(highestScore === 0 || client.score > highestScore) {
			winnerClient = client;
		}
	}
	return winnerClient;
};

// set a client's current round value
ClientManager.prototype.setClientRound = function(socketId, round) {
	// client not found
	if(typeof this.clients[socketId] === 'undefined') {
		return false;
	}
	
	this.clients[socketId].current_round = round;
	return true;
};

// set a client's score value
ClientManager.prototype.setClientScore = function(socketId, score) {
	// client not found
	if(typeof this.clients[socketId] === 'undefined') {
		return false;
	}
	
	this.clients[socketId].score = score;
	return true;
};

// add a client
ClientManager.prototype.addClient = function(client) {
	// add client to either player or spectator list
	if(typeof this.clients[client.socket.id] === 'undefined') {
		client.score         = client.score || 0;
		client.current_round = client.current_round || 1;

		this.clients[client.socket.id] = client;
		return true;
	}
	return false;
};

// remove a client using socket id
ClientManager.prototype.removeClient = function(socketId) {
	if(typeof this.clients[socketId] !== 'undefined') {
		delete this.clients[socketId];
		return true;
	}

	return false;
};


module.exports = ClientManager;