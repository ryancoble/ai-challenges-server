'use strict';

let fs = require('fs');

const CONFIG_PATH = './config.json';

function Config() {
	this.configData = null;

	// config file not found
	if(!fs.existsSync(CONFIG_PATH)) {
		throw new Error('Config: file not found!');
	}

	// synchronously read config data from a json file
	let rawConfigData = fs.readFileSync(CONFIG_PATH, 'utf8');
	this.configData = JSON.parse(rawConfigData);
}

// get a configuration (based on a '.' separated/encoded string)
Config.prototype.get = function(dotEncodedString) {

	// no key given
	if(!dotEncodedString.length) {
		throw new Error('Config: no key given!');
	}

	// get all configs
	if(dotEncodedString === 'all') {
		return this.configData;
	}

	// top level (no '.' found)
	if(dotEncodedString.indexOf('.') === -1) {
		// config not found
		if(typeof this.configData[dotEncodedString] === 'undefined') {
			throw new Error('Config: key \"' + dotEncodedString + '\" not found!');
		}
		return this.configData[dotEncodedString];
	}

	let encodedStringParts = dotEncodedString.split('.');

	let data = this.configData;
	for(let iPart = 0; iPart < encodedStringParts.length; iPart++) {
		let stringPart = encodedStringParts[iPart];

		// config not found
		if(typeof data[stringPart] === 'undefined') {
			throw new Error('Config: key \"' + dotEncodedString + '\" not found!');
		}

		data = data[stringPart];
	}
	return data;
};


/**
 * SINGLETON - one instance
 */

// singleton pattern to maintain state
let ConfigSingleton = (function() {
	let instance;

	function createInstance() {
		return new Config();
	}

	return {
		getInstance: function() {
			if(!instance) {
				instance = createInstance();
			}
			return instance;
		}
	};
})();

module.exports = ConfigSingleton;