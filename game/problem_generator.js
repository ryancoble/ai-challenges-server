var generators = {
	xor: function(numOfInputs) {
		var problem = {};
		
		// two random binary inputs
		input1 = (Math.floor(Math.random() * 9) % 2);
		input2 = (Math.floor(Math.random() * 9) % 2);

		// calculate out the XOR result
		if(input1 != input2) output = 1;
		else output = 0;

		problem = {
			inputs: [input1, input2],
			target_output: output
		};
		return problem;
	}
};


function ProblemGenerator() {}

ProblemGenerator.prototype.generate = function(challengeKey) {

	if(typeof generators[challengeKey] === 'undefined') {
		throw new Error('Problem Generator: \"' + challengeKey + '\" is not supported!');
		return ;
	}

	var gen = generators[challengeKey];
	return gen();
};

module.exports = new ProblemGenerator();