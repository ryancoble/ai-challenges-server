'use strict';

const challengesModel = require('./../models/challenges');

function ChallengeRepository() {
	this.model = challengesModel;
}

ChallengeRepository.prototype.getAll = function() {
	const self = this;
	return new Promise(function(resolve, reject) {
		self.model.find(function(err, challenges) {
			if(err) {
				reject(err);
				return ;
			}

			resolve(challenges);
		});
	});
};

// get a challenge by object
ChallengeRepository.prototype.get = function(queryObj) {
	const self = this;
	return new Promise(function(resolve, reject) {
		self.model.find(queryObj, function(err, challenges) {
			console.log(challenges);
			if(err || !challenges) {
				reject(err || 'No challenges found.');
				return ;
			}

			resolve(challenges);
		});
	});
};

// get a challenge by key
ChallengeRepository.prototype.getByKey = function(challenge_key) {
	const self = this;
	return new Promise(function(resolve, reject) {
		self.model.findOne({key: challenge_key}, function(err, challenge) {
			if(err) {
				reject(err);
				return ;
			}

			resolve(challenge);
		});
	});
};

// update a challenge by key
ChallengeRepository.prototype.updateByKey = function(challenge_key, updatedChallenge) {
	const self = this;
	return new Promise(function(resolve, reject) {
		self.model.findOne({key: challenge_key}, function(err, challenge) {
			if(err) {
				reject(err);
				return;
			}

			self.model.update({key: challenge_key}, updatedChallenge, function(err, challenge) {
				if(err) {
					reject(err);
					return;
				}

				resolve(challenge);
			});
		})
	})
};

module.exports = new ChallengeRepository();