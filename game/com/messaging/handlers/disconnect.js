'use strict'

/**
 * HANDLE DISCONNECT MESSAGE
 * duties: set client type
 */
const ColorLog     = require('color-logs');
const log          = ColorLog(true, true, 'Messaging');
const gameInstance = require('./../../../game').getInstance();

// handle handshake message
module.exports = function(socket) {

	function handle(message) {
		// remove client from list
		gameInstance.clientManager.removeClient(socket.id);
		log.info('client[' + socket.id + ']: handle disconnect message');

		// add update to the queue
		let update = gameInstance.updateManager.makeUpdate('disconnect', {
			winner_id: winnerClient.socket.id,
			winner_score: winnerClient.score
		});
		gameInstance.updateManager.push(update);
	}

	return {
		handle: handle
	};
};