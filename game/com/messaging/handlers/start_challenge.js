'use strict';

/**
 * HANDLE START CHALLENGE MESSAGE
 * duties: start challenge
 */
const ColorLog         = require('color-logs');
const log              = ColorLog(true, true, 'Messaging');
const gameInstance     = require('./../../../game').getInstance();
const messaging        = require('./../server_messaging');
const challengeRepo    = require('./../../../repositories/challenge_repository');
const problemGenerator = require('./../../../problem_generator');

// handle start challenge message
module.exports = function(socket) {

	function validate(messageBody) {
		// message does not have a challenge key
		if(typeof messageBody.challenge_key === 'undefined') {
			return false;
		}
		return true;
	}

	function handle(messageBody) {
		// get current challenge from the repo
		challengeRepo.getByKey(messageBody.challenge_key).then(function(challenge) {
			// check if challenge is already started
			if(challenge.status !== 'pending') {
				console.log('challenge already started');
				return ;
			}

			// set challenge to status started
			challenge.status = 'started';
			challengeRepo.updateByKey(challenge.key, challenge);

			// generate and set the problems for each round
			let rounds = [];
			for(let i = 0; i < challenge.rounds; i++) {
				let problems = [];
				for(let j = 0; j < challenge.iterations_per_round; j++) {
					problems.push(problemGenerator.generate(challenge.key));
				}
				rounds.push(problems);
			}
			gameInstance.challengeManager.setupChallenge(challenge.key, rounds);

			// get problems for the round
			let problems = gameInstance.challengeManager.getProblemSet(challenge.key, challenge.current_round);

			// send start challenge message to all clients
			let updateRoundMessage = messaging.makeMessage('round_update', {
				challenge_key: challenge.key,
				problems:      problems
			});
			// get all the competitor clients of the current challenge
			let competitors = gameInstance.clientManager.getCompetitors(challenge.key);
			// send first round update data to all competitor clients
			for(let clientIndex in competitors) {
				messaging.sendMessage(competitors[clientIndex].socket, updateRoundMessage);
			}
			log.info('challenge started!');

			// add update to the queue
			let update = gameInstance.updateManager.makeUpdate('start_challenge', {
				starting_client_id: socket.id,
				challenge_key: challenge.key,
				rounds: challenge.rounds,
				iterations_per_round: challenge.iterations_per_round,
				problems: gameInstance.challengeManager.getChallengeByKey(challenge.key).problems
			});
			gameInstance.updateManager.push(update);
		});
	}

	return {
		validate: validate,
		handle: handle
	};
};