'use strict';

/**
 * HANDLE END CHALLENGE MESSAGE
 * duties: end challenge
 */

const gameInstance = require('./../../../game').getInstance();
//let messaging = require('./../server_messaging');
const challengeRepo = require('./../../../repositories/challenge_repository');

// handle end challenge message
module.exports = function(socket) {

	function validate(messageBody) {
		// message does not have a challenge key
		if(typeof messageBody.challenge_key === 'undefined') {
			return false;
		}
		return true;
	}

	function handle(messageBody) {
		// remove client from list
		// gameInstance.clientManager.removeClient(socket.socketId);
	}

	return {
		validate: validate,
		handle: handle
	};
};