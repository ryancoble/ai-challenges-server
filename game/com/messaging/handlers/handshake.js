'use strict';

/**
 * HANDLE HANDSHAKE MESSAGE
 * duties: set client type, current challenge
 */
const ColorLog      = require('color-logs');
const log           = ColorLog(true, true, 'Messaging');
const gameInstance  = require('./../../../game').getInstance();
const challengeRepo = require('./../../../repositories/challenge_repository');

// handle handshake message
module.exports = function(socket) {

	function validate(messageBody) {
		// message does not have a client type
		if(typeof messageBody.client_type === 'undefined') {
			return false;
		}
		// message does not have a challenge key
		if(typeof messageBody.challenge_key === 'undefined') {
			return false;
		}
		return true;
	}

	function handle(messageBody) {
		let clientType = messageBody.client_type.toLowerCase();
		challengeRepo.getByKey(messageBody.challenge_key).then(function(challenge) {
			// challenge not already added, so add it
			if(!gameInstance.challengeManager.getChallengeByKey(challenge.key)) {
				gameInstance.challengeManager.addChallenge(challenge);
			}

			// add client to client list, set current challenge and set client type
			let client = {
				socket:        socket,
				type:          clientType,
				challenge_key: challenge.key
			};
			gameInstance.clientManager.addClient(client);

			// add update to the queue
			let update = gameInstance.updateManager.makeUpdate('add_client', {
				challenge_key: challenge.key,
				client_type: clientType,
				client_id: socket.id
			});
			gameInstance.updateManager.push(update);

		}).catch(function(err) {
			log.error(err);
		});
	}

	return {
		validate: validate,
		handle: handle
	};
};