'use strict';

/**
 * HANDLE START CHALLENGE MESSAGE
 * duties: start challenge
 */
const ColorLog         = require('color-logs');
const log              = ColorLog(true, true, 'Messaging');
const gameInstance     = require('./../../../game').getInstance();
const messaging        = require('./../server_messaging');
const challengeRepo    = require('./../../../repositories/challenge_repository');
const problemGenerator = require('./../../../problem_generator');

// handle update round on a challenge message
module.exports = function(socket) {

	function validate(messageBody) {
		// message does not have a challenge key
		if(typeof messageBody.challenge_key === 'undefined') {
			return false;
		}
		// message does not have a problems array
		if(typeof messageBody.problems === 'undefined') {
			return false;
		}
		// message does not have a problems array of object with a predicted_output property
		for(let problemIndex in messageBody.problems) {
			let problem = messageBody.problems[problemIndex];
			if(typeof problem.predicted_output === 'undefined') {
				return false;
			}
		}
		return true;
	}

	function handle(messageBody) {
		// get current challenge from the repo
		challengeRepo.getByKey(messageBody.challenge_key).then(function(challenge) {
			let client = gameInstance.clientManager.getClient(socket.id);

			// increment the current client current round value (competitors only)
			if(client.type === 'competitor') {
				// calculate the score by summing up the predicted outputs and subtracting by the sum of the target outputs
				let predictedSum = 0;
				let targetSum = 0;
				for(let problemIndex in messageBody.problems) {
					predictedSum += messageBody.problems[problemIndex].predicted_output;
					targetSum    += messageBody.problems[problemIndex].target_output;
				}
				// set the score for the current client
				gameInstance.clientManager.setClientScore(socket.id, targetSum - predictedSum);
				client.score = targetSum - predictedSum;

				// set the round for the current client
				gameInstance.clientManager.setClientRound(socket.id, challenge.current_round);
				client.current_round = challenge.current_round;

				// add update to the queue
				let update = gameInstance.updateManager.makeUpdate('competitor_update', {
					challenge_key: challenge.key,
					client_id: client.socket.id,
					score: client.score,
					current_round: client.current_round,
					problems_with_predictions: messageBody.problems
				});
				gameInstance.updateManager.push(update);
			}


			/**
			 * ROUND COMPLETE
			 */

			// get all competitors for the given challenge
			// check if all competitors have completed this round
			let isRoundComplete = false;
			let competitors = gameInstance.clientManager.getCompetitors(challenge.key);
			for(let competitorIndex in competitors) {
				if(competitors[competitorIndex].current_round === challenge.current_round) {
					isRoundComplete = true;
				}
			}

			// when all competitors have answered
			if(isRoundComplete) {

				/**
				 * END GAME
				 */

				// check if all competitors have responded this round and current round is passed the number of rounds for the challenge
				if(challenge.current_round >= challenge.rounds) {
					// get the winner of the challenge
					let winnerClient = gameInstance.clientManager.getWinner(challenge.key);

					// send the end_challenge message to all clients
					let endChallengeMessage = messaging.makeMessage('end_challenge', {
						challenge_key: challenge.key,
						winner_id:     winnerClient.socket.id
					});
					// send to all competitors of the challenges
					let clients = gameInstance.clientManager.getCompetitors(challenge.key);
					for(let clientIndex in clients) {
						messaging.sendMessage(clients[clientIndex].socket, endChallengeMessage);
					}

					// set the current round back to 1
					challenge.status        = 'pending';
					challenge.current_round = 1;
					challengeRepo.updateByKey(challenge.key, challenge).then(function(challenge) {
						// challenge completed
						log.info('challenge completed!');
						console.log('winner score: '  + winnerClient.score);
						console.log('winner id: ' + winnerClient.socket.id);
					});

					// add update to the queue
					let update = gameInstance.updateManager.makeUpdate('end_challenge', {
						challenge_key: challenge.key,
						winner_id: winnerClient.socket.id,
						winner_score: winnerClient.score
					});
					gameInstance.updateManager.push(update);

					return;
				}

				// increment current challenge's current round by 1, when all competitors have answered
				challenge.current_round++;
				challengeRepo.updateByKey(challenge.key, challenge);
			}

			// only send update to competitors that have not answered this round
			if(client.type === 'competitor' && (client.current_round + 1) == challenge.current_round && challenge.current_round <= challenge.rounds) {
				// send next round update to client
				let problemSet = gameInstance.challengeManager.getProblemSet(challenge.key, client.current_round + 1);
				let updateRoundMessage = messaging.makeMessage('round_update', {
					challenge_key: challenge.key,
					problems:      problemSet
				});
				messaging.sendMessage(socket, updateRoundMessage);
			}
		});
	}

	return {
		validate: validate,
		handle: handle
	};
};