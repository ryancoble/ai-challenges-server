'use strict';

// a message instance
function Message(messageId, body) {
	return {
		message_id: messageId,
		body: body
	};
}

module.exports = Message;