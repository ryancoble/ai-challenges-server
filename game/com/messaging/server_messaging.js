'use strict';

const ColorLog = require('color-logs');
const log      = ColorLog(true, true, 'Messaging');
const Message  = require('./message');

function Messaging() {}

// create a message
Messaging.prototype.makeMessage = function(messageId, messageBody) {
	return new Message(messageId, messageBody);
};

// validate a message's global structure
Messaging.prototype.validateMessage = function(message) {
	// message does not have a message id
	if(typeof message.message_id === 'undefined') {
		return false;
	}
	// message does not have a message body
	if(typeof message.body === 'undefined') {
		return false;
	}
	return true;
};

// handle a message
Messaging.prototype.handleMessage = function(socket, message) {
	try {
		// validate the global structure of the message
		if(!this.validateMessage(message)) {
			throw new Error('message global structure invalid');
		}

		// get the message handler for the given message id and pass it the current socket
		let handler = require('./handlers/' + message.message_id.toLowerCase())(socket);

		// validate the structure of the message specifically for the message id
		if(!handler.validate(message.body)) {
			throw new Error('message structure invalid for message id \"' + message.message_id + '\"');
		}

		// handle the message
		handler.handle(message.body);
	}
	catch(e) {
		log.error('Message Handling: ' + e);
	}
};

// send a message
Messaging.prototype.sendMessage = function(socket, message) {
	try {
		// validate the global structure of the message
		if(!this.validateMessage(message)) {
			throw new Error('message global structure invalid');
		}

		socket.emit('message', message);
	}
	catch(e) {
		log.error('Message Sending: ' + e);
	}
};


module.exports = new Messaging();