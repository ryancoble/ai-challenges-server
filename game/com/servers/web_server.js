'use strict';

const ColorLog = require('color-logs');
const log = ColorLog(true, true, /*__filename*/'WebServer');
const challengeRepo = require('./../../repositories/challenge_repository');


// where all the routes go
function setupRoutes(app) {
	// the panel dashboard (list challenges)
	app.get('/', function(req, res) {

		// get all challenges from repo
		challengeRepo.getAll().then(function(challenges) {
			if(!challenges) {
				res.send('<p>No challenges found</p>');
			}
			res.render('dashboard', {
				title: 'Challenge list',
				challenges: challenges
			});
		});
	});

	// challenge page
	app.get('/challenges/:key', function(req, res) {
		let key = req.params.key
		
		// no key given
		if(!key) {
			res.send('<p>No challenge key given!</p>');
			return;
		}

		// get challenge by key from repo
		challengeRepo.getByKey(key).then(function(challenge) {
			if(!challenge) {
				res.send('<p>Challenge not found!</p>');
				return;
			}

			res.render('challenge', {
				title: challenge.key + ' Challenge',
				challenge: challenge
			});
		}).catch(function(err) {
			res.send('<p>' + err.toString() + '</p>');
		});
	});

	// testing graphing
	app.get('/test', function(req, res) {
		res.render('graph_test', {
			title: 'Challenge dashboard',
			challenge: 'xor'.toUpperCase()
		});
	});
}

function WebServer(app, port) {
	this.app  = app;
	this.port = port;
}

WebServer.prototype.start = function() {

	setupRoutes(this.app);

	// start listening webserver on port 9000
	let webServer = this.app.listen(this.port, function() {
		log.info('web server: running on port ' + webServer.address().port);
	});

	return webServer;
};

module.exports = WebServer;