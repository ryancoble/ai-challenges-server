'use strict';

const io = require('socket.io');
const ColorLog = require('color-logs');
const log = ColorLog(true, true, 'MessageServer');
const configInstance = require('./../../config').getInstance();
const messaging = require('./../../com/messaging/server_messaging');
const challengeRepo = require('./../../repositories/challenge_repository');

function MessageServer(httpServer, port) {
	let self = this;
	this.httpServer = httpServer;
	this.port    = port;
	this.clients = {};
}

// start the listening for connections
MessageServer.prototype.start = function() {
	let self = this;

	// create socket server instance
	this.socketServer = io.listen(this.port, this.httpServer);
	log.info('listening on port ' + this.port);

	// when a client connects
	this.socketServer.sockets.on('connection', function(clientSocket) {

		/**
		 * CLIENT CONNECT
		 */
		// add client socket to list of connected clients
		self.clients[clientSocket.id] = clientSocket;
		log.info('client[' + clientSocket.id + '] connected!');


		/**
		  * SOCKET EVENTS
		  */
		// on message sent from client
		clientSocket.on('message', (message) => {
			// pass the raw message to messaging module to be handled
			messaging.handleMessage(clientSocket, message);
			log.info('client[' + clientSocket.id + '] sent: ' + JSON.stringify(message));
		});
		// on client disconnect
		clientSocket.on('end', function() {
			// remove client socket from list of connected clients
			delete self.clients[clientSocket.id];
			
			// handle the message
			messaging.handleMessage(clientSocket, {
				message_id: 'disconnect'
			});
			log.info('client[' + clientSocket.id + '] disconnected!');
		});


		/**
		  * HANDSHAKE
		  */
		// get all challenges from the repo
		challengeRepo.getAll().then(function(challenges) {
			// send handshake message, for client to select a challenge and let us know
			// whether the client is a player or a spectator
			const handshakeMessage = messaging.makeMessage('handshake', {
				challenges: challenges
			});
			messaging.sendMessage(clientSocket, handshakeMessage);
			log.info('client[' + clientSocket.id + '] send handshake!');
		}).catch(function(err) {
			log.error('Challenge Repository: ' + err);
		});
	});
};

module.exports = MessageServer;