'use strict'

let WebServer     = require('./servers/web_server');
let MessageServer = require('./servers/message_server');

function ServerLauncher(app, serversConfig) {
	this.app    = app;
	this.config = serversConfig;
}

// launch all the servers (web and message)
ServerLauncher.prototype.launch = function() {
	// WEB SERVER
	let webServer = new WebServer(this.app, this.config.web.port);
	let httpServer = webServer.start();

	// MESSAGE SERVER
	let messageServer = new MessageServer(httpServer, this.config.message.port);
	messageServer.start();
};

module.exports = ServerLauncher;