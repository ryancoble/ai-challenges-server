'use strict';

const exitHook         = require('exit-hook');
const ServerLauncher   = require('./com/server_launcher');
const configInstance   = require('./config').getInstance();
const mongoose         = require('mongoose');
const messaging        = require('./com/messaging/server_messaging');
const ChallengeManager = require('./managers/challenge_manager');
const ClientManager    = require('./managers/client_manager');
const UpdateManager    = require('./managers/update_manager');
const challengeRepo    = require('./repositories/challenge_repository');

function Game() {
	this.config = null;
 
	this.challengeManager = new ChallengeManager();
	this.clientManager    = new ClientManager();
	this.updateManager    = new UpdateManager();

	this.uiUpdateTimer = null;
}

// start the game
// - start up all the servers
// - 
Game.prototype.start = function(app) {
	let self = this;

	// get the configurations
	this.config = configInstance.get('all');

	// connect to the database
	mongoose.connect(this.config.database.url);

	// Launch all our servers
	let serverLauncher = new ServerLauncher(app, this.config.servers);
	serverLauncher.launch();

	/**
	 * UI UPDATE
	 * - update all UI clients(spectators) with data to render view
	 */
	this.uiUpdateTimer = setInterval(function() {

		// get all challenge update from a queue
		let challengeUpdate = self.updateManager.pop();

		if(!challengeUpdate.update_key) {
			return;
		}

		// loop over all challenge updates
		//for(let iUpdateIndex = 0; iUpdateIndex < challengeUpdates.length; iUpdateIndex) {
			//let challengeUpdate = challengeUpdates[iUpdateIndex];

			// make update message
			let updateUIMessage = messaging.makeMessage('update_ui', challengeUpdate);
			// get all spectators of the given update's challenge
			let spectators = self.clientManager.getSpectators(challengeUpdate.data.challenge_key);
			for(let iSpectator in spectators) {
				// send update message
				messaging.sendMessage(spectators[iSpectator].socket, updateUIMessage);
			}
		//}

	}, configInstance.get('game.update_interval'));
};

// end the game
Game.prototype.end = function() {
	// set UI update loop
	clearInterval(this.uiUpdateTimer);
	this.uiUpdateTimer = null;

	// reset all started challenges back to original state
	challengeRepo.get({"status": "started"}).then(function(challenges) {
		console.log('test');
		console.log(challenges);
		for(let challengeIndex in challenges) {
			let challenge = challenges[challengeIndex];
			challenge.current_round = 1;
			challenge.status        = 'pending';
			challengeRepo.updateByKey(challenge.key, challenge);
		}
	}).catch(function(err) {
		console.log(err);
	});
};


/**
 * SINGLETON - one instance
 */

// singleton pattern to maintain state
let GameSingleton = (function() {
	let instance;

	function createInstance() {
		return new Game();
	}

	return {
		getInstance: function() {
			if(!instance) {
				instance = createInstance();

				// when process ends
				exitHook(function() {
					instance.end();
				});
			}
			return instance;
		}
	};
})();

module.exports = GameSingleton;