'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const challengesSchema = new Schema({
	key: String,
	iterations_per_round: Number,
	current_round: Number,
	status: String,
	rounds: Number,
	problems: Array
});
mongoose.model('challenges', challengesSchema);

module.exports = mongoose.model('challenges');