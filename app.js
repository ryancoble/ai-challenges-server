'use strict';

let express         = require('express');
let mustacheExpress = require('mustache-express');
let bodyParser      = require('body-parser');
let path            = require('path');
let gameInstance    = require('./game/game').getInstance();

var app = express();

// set templating
app.engine('mustache', mustacheExpress(path.join(__dirname, 'views')));
app.set('view engine', 'mustache');
app.set('views', path.join(__dirname, 'views'));
app.disable('view cache');

// middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

// start the game
gameInstance.start(app);