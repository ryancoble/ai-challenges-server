var gulp = require('gulp');
var concat = require('gulp-concat');

var scriptFiles = [
    'public/js/helpers/*.js',
    'public/js/init.js'
];

gulp.task('js', function() {
    gulp.src(scriptFiles)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('public/build/js'));
});

gulp.task('default', function() {

    gulp.watch(scriptFiles, function(event) {
        gulp.run('js');
    });
});